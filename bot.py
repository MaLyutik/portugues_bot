from telebot import types
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import Filters, MessageHandler
from telegram.ext import Updater, CallbackContext

from libs import app


# util
def id_from_update(update):
    return update.message.chat.id


# keyboard
def bot_add_new_word(keyboard):
    keyboard[0].append(InlineKeyboardButton(text='Добавим новые слова вручную', callback_data='new'))


def bot_train_option(keyboard):
    keyboard[0].append(InlineKeyboardButton(text="Учить слова", callback_data='train'))


def bot_show_settings_prompt(keyboard):
    keyboard[0].append(InlineKeyboardButton(text='Показать настройки', callback_data='show_settings'))


def add_new_word_prompt(bot, chat_id):
    bot.send_message(chat_id, "Введи через запятую:слово,произношение,перевод")


# handlers
def start(bot, update):
    keyboard = [[]]
    user_id = update.message.chat.id
    user_first_name = update.message.chat.first_name
    has_words_to_train = app.start(user_id, user_first_name)
    if has_words_to_train:
        bot_train_option(keyboard)
    else:
        add_existing_prompt = types.InlineKeyboardButton(text='Скопировать текущую версию словаря для тренировок',
                                                         callback_data='add_existing')
        keyboard.append(add_existing_prompt)
    bot_add_new_word(keyboard)
    bot_show_settings_prompt(keyboard)
    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text(
        f"{user_first_name}, что будем делать?",
        reply_markup=reply_markup
    )


def handle_text(bot, update):
    print("handling test")
    message = update.message
    print("{message}")
    message_split = message.text.split(",")
    result = app.handle_user_response(message_split, message.from_user.id, message.text.lower())
    if len(message_split) == 3:
        add_new_word_prompt(message.chat)
    if result is not None:
        bot.send_message(message.chat.id, result.message)
        if result.show_word_prompt:
            bot.send_message(message.chat.id, app.train_next_word_prompt(message.from_user.id))


def train(update: Update, context: CallbackContext) -> None:
    print(f"{update}")
    print(f"{context}")
    query = context.callback_query
    print(f"{query.message.chat.id}")
#    query.answer()
    user_id = id_from_update(query)
    print(f"{user_id}")
    app.train(user_id)
#    query.answer(text=app.train_next_word_prompt(user_id))
#    context.message.reply_text(app.train_next_word_prompt(user_id))
    # bot.send_message(user_id, app.train_next_word_prompt(user_id))
    # query
#todo: make a normal reply
    query.edit_message_text(text=app.train_next_word_prompt(user_id))


def new(update: Update, context: CallbackContext) -> None:
    query = context.callback_query
    query.answer()
    add_new_word_prompt(id_from_update(query))


def add_existing(bot, update):
    user_id = id_from_update(update)
    app.add_existing_words_for_user_training(user_id)
    keyboard = [[]]
    bot_train_option(keyboard)
    bot_add_new_word(keyboard)
    bot.send_message(user_id, text="Готово! Что дальше?", reply_markup=keyboard)


def show_settings(bot, update):
    user_id = id_from_update(update)
    settings = app.get_settings(user_id)
    bot.send_message(user_id, text=f"Количество слов в одной учебной сессии {settings[2]}")


# run
def token():
    fh = open("telegram.token")
    return fh.readline().rstrip()


WEBHOOK_HOST = '194.87.93.93'
WEBHOOK_PORT = 8443

WEBHOOK_SSL_CERT = '/home/bot/portugues_bot/my_cert.pem'  # Путь к сертификату
WEBHOOK_SSL_PRIV = '/home/bot/portugues_bot/my_private_key.pem'  # Путь к приватному ключу

WEBHOOK_URL_PATH = "/%s/" % (token())

updater = Updater(token=token(), use_context=False)

updater.dispatcher.add_handler(CommandHandler(['start', 'restart'], start))
updater.dispatcher.add_handler(CommandHandler(['train'], train))
updater.dispatcher.add_handler(CommandHandler(['new'], new))
updater.dispatcher.add_handler(CommandHandler(['show_settings'], show_settings))
updater.dispatcher.add_handler(MessageHandler(Filters.text, handle_text))
updater.dispatcher.add_handler(CallbackQueryHandler(train))

if __name__ == "__main__":
    updater.start_webhook(
        listen=WEBHOOK_HOST,
        port=WEBHOOK_PORT,
        url_path=WEBHOOK_URL_PATH,
        cert=WEBHOOK_SSL_CERT,
        key=WEBHOOK_SSL_PRIV
    )
