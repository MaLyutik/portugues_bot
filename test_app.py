from fuzzywuzzy import fuzz as fuzzy_search
from telebot import types
from libs import database_operations as database
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import Filters, MessageHandler


def token():
    fh = open("telegram.token")
    return fh.readline().rstrip()


WEBHOOK_HOST = '194.87.93.93'
WEBHOOK_PORT = 8443

WEBHOOK_SSL_CERT = '/home/bot/portugues_bot/my_cert.pem'  # Путь к сертификату
WEBHOOK_SSL_PRIV = '/home/bot/portugues_bot/my_private_key.pem'  # Путь к приватному ключу

WEBHOOK_URL_PATH = "/%s/" % (token())

updater = Updater(token=token(), use_context=False)


def start(bot, update):
    print("We've received update")
    # print(update)
    # print(update.message)
    # bot.sendMessage(chat_id=update.message.chat_id, text="Здравствуйте.")

def handle_text(bot, update):
    print("Handle text")

def handle_command(bot, update):
    print("Handle command")


start_handler = CommandHandler(['start', 'restart'], start)
text_handler = MessageHandler(Filters.text, handle_text)


updater.dispatcher.add_handler(start_handler)
updater.dispatcher.add_handler(text_handler)

def bot_add_new_word(keyboard):
    keyboard.add(types.InlineKeyboardButton(text='Добавим новые слова вручную', callback_data='new'))


if __name__ == "__main__":
    updater.start_webhook(
        listen=WEBHOOK_HOST,
        port=WEBHOOK_PORT,
        url_path=WEBHOOK_URL_PATH,
        cert=WEBHOOK_SSL_CERT,
        key=WEBHOOK_SSL_PRIV
    )

